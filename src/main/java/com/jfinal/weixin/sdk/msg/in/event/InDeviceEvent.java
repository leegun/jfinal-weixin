/**
 * Copyright (c) 2015-2016, Javen Zhou  (javen205@126.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */
package com.jfinal.weixin.sdk.msg.in.event;
/**
 * <pre>
	设备备绑定/解绑事件
&lt;xml&gt;
	&lt;ToUserName&gt;&lt;![CDATA[%s]]&gt;&lt;/ToUserName&gt;
	&lt;FromUserName&gt;&lt;![CDATA[%s]]&gt;&lt;/FromUserName&gt;
	&lt;CreateTime&gt;%u&lt;/CreateTime&gt;
	&lt;MsgType&gt;&lt;![CDATA[%s]]&gt;&lt;/MsgType&gt;
	&lt;Event&gt;&lt;![CDATA[%s]]&gt;&lt;/Event&gt;
	&lt;DeviceType&gt;&lt;![CDATA[%s]]&gt;&lt;/DeviceType&gt;
	&lt;DeviceID&gt;&lt;![CDATA[%s]]&gt;&lt;/DeviceID&gt;
	&lt;Content&gt;&lt;![CDATA[%s]]&gt;&lt;/Content&gt;
	&lt;SessionID&gt;%u&lt;/SessionID&gt;
	&lt;OpenID&gt;&lt;![CDATA[%s]]&gt;&lt;/OpenID&gt;
&lt;/xml&gt;
Event事件类型，取值为bind/unbind bind：绑定设备 unbind：解除绑定
 * </pre>
 */
public class InDeviceEvent extends EventInMsg {
	public static final String EVENT_INDEVICE_BIND = "bind";
	public static final String EVENT_INDEVICE_UNBIND = "unbind";
	
	private String deviceType;
	private String deviceId;
	private String content;
	private String sessionId;
	private String openId;
	
	
	public InDeviceEvent(String toUserName, String fromUserName, Integer createTime, String msgType, String event) {
		super(toUserName, fromUserName, createTime, msgType, event);
	}


	public String getDeviceType() {
		return deviceType;
	}


	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}


	public String getDeviceId() {
		return deviceId;
	}


	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}


	public String getContent() {
		return content;
	}


	public void setContent(String content) {
		this.content = content;
	}


	public String getSessionId() {
		return sessionId;
	}


	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}


	public String getOpenId() {
		return openId;
	}


	public void setOpenId(String openId) {
		this.openId = openId;
	}
	
}
