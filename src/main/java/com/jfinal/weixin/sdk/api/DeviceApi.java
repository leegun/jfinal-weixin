/**
 * Copyright (c) 2015-2016, Javen Zhou  (javen205@126.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */

package com.jfinal.weixin.sdk.api;

import java.util.HashMap;
import java.util.Map;

import com.jfinal.kit.HttpKit;
import com.jfinal.weixin.sdk.utils.HttpUtils;

/**
 * 主动发送消息给设备
 * @author Javen
 * 2016年6月8日
 * 参考地址：http://iot.weixin.qq.com/wiki/document-2_3.html
 */
public class DeviceApi {
	private static String ctrl_device = "https://api.weixin.qq.com/hardware/mydevice/platform/ctrl_device?access_token=";
	
	public static ApiResult ctrlDevice(String jsonStr) {
		String url=ctrl_device + AccessTokenApi.getAccessTokenStr();
		System.out.println(url);
		String jsonResult = HttpUtils.post(url, jsonStr);
		return new ApiResult(jsonResult);
	}
	
	//绑定成功通知>第三方后台绑定操作处理成功，通知公众平台。
	private static String bindSuccessUrl = "https://api.weixin.qq.com/device/bind?access_token=";
	//解绑成功通知>第三方确认用户和设备的解绑操作。
	private static String unBindSuccessUrl = "https://api.weixin.qq.com/device/unbind?access_token=";
	//强制绑定用户和设备>第三方强制绑定用户和设备。
	private static String compelBindUrl = "https://api.weixin.qq.com/device/comple_bind?access_token=";
	//强制解绑用户和设备>第三方强制解绑用户和设备。
	private static String compelUnBindUrl = "https://api.weixin.qq.com/device/comple_unbind?access_token=";
	//第三方主动发送设备状态消息给微信终端>第三方发送设备状态消息给设备主人的微信终端。
	private static String transMsgUrl = "https://api.weixin.qq.com/device/transmsg?access_token=";
	
	
	
	//设备状态查询
	private static String getStatUrk="https://api.weixin.qq.com/device/get_stat";
	
	/**
	 * 设备状态查询
	 * @param deviceId
	 * @return
	 */
	public static ApiResult getStat(String deviceId){
		Map<String, String> queryParas= new HashMap<String, String>();
		queryParas.put("access_token", AccessTokenApi.getAccessTokenStr());
		queryParas.put("device_id", deviceId);
		return new ApiResult(HttpKit.get(getStatUrk, queryParas));
	}
	
	public enum DeviceEnum{
		bindSuccss,unBindsuccess,compelBind,compelUnBind,transMsg
	}
	/**
	 * @param jsonStr
	 * @param deviceBind
	 * @return
	 */
	public static ApiResult bindOrUnbind(String jsonStr,DeviceEnum deviceEnum) {
		String url = null;
		String bindNmae=deviceEnum.name();
		if (bindNmae.equals("bindSuccss")) {
			url = bindSuccessUrl;
		}else if (bindNmae.equals("unBindsuccess")) {
			url = unBindSuccessUrl;
		}else if (bindNmae.equals("compelBind")) {
			url = compelBindUrl;
		}else if (bindNmae.equals("compelUnBind")) {
			url = compelUnBindUrl;
		}else if (bindNmae.equals("transMsg")) {
			url = transMsgUrl;
		}
		String jsonResult = HttpUtils.post(url + AccessTokenApi.getAccessTokenStr(), jsonStr);
		return new ApiResult(jsonResult);
	}
}
